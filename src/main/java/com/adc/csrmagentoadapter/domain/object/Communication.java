package com.adc.csrmagentoadapter.domain.object;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class Communication {

    @NotNull
    @JsonProperty("leadId")
    private Integer leadId;
    @NotNull
    @JsonProperty("csrCustomerId")
    private Integer csrCustomerId;
    @NotNull
    @Size(max = 150)
    @JsonProperty("email")
    private String email;
    @NotNull
    @Size(max = 255)
    @JsonProperty("communicationType")
    private String communicationType;
    @NotNull
    @Size(max = 255)
    @JsonProperty("communicationChannel")
    private String communicationChannel;
    @NotNull
    @Size(max = 10)
    @JsonProperty("sourceSystem")
    private String sourceSystem;
    @JsonProperty("creationDate")
    private String creationDate;
    @Size(max = 255)
    @JsonProperty("permisionCaptureMethod")
    private String permisionCaptureMethod;
    @NotNull
    @JsonProperty("permissionStatus")
    private Integer permissionStatus;
    @JsonProperty("permissionDate")
    private String permissionDate;
}
