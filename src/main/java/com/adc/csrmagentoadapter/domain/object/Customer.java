package com.adc.csrmagentoadapter.domain.object;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
public class Customer {
    @NotNull
    @JsonProperty("csrCustomerId")
    private String csrCustomerId;
    @NotNull
    @Size(max = 255)
    @JsonProperty("prefix")
    private String prefix;
    @NotNull
    @Size(max = 255)
    @JsonProperty("firstName")
    private String firstName;
    @NotNull
    @Size(max = 255)
    @JsonProperty("lastName")
    private String lastName;
    @NotNull
    @Size(max = 255)
    @JsonProperty("emailId")
    private String emailId;
    @JsonProperty("isActive")
    private Boolean isActive;
    @NotNull
    @Size(max = 255)
    @JsonProperty("groupId")
    private String groupId;
    @JsonProperty("dob")
    private String dob;
    @Size(max = 255)
    @JsonProperty("customerLandline")
    private String customerLandline;
    @Size(max = 255)
    @JsonProperty("customerMobile")
    private String customerMobile;
    @NotNull
    @JsonProperty("createdAt")
    private String createdAt;
    @NotNull
    @JsonProperty("updatedAt")
    private String updatedAt;
    @Size(max = 255)
    @JsonProperty("fiscalCode")
    private String fiscalCode;
    @Size(max = 1)
    @JsonProperty("invitedCustomerStatus")
    private String invitedCustomerStatus;
    @NotNull
    @JsonProperty("invitedCustomerStatusDate")
    private String invitedCustomerStatusDate;
    @JsonProperty("payerInstitution")
    private Integer payerInstitution;
    @NotNull
    @JsonProperty("ghostAccountId")
    private Integer ghostAccountId;
    @NotNull
    @JsonProperty("insuranceNumber")
    private Integer insuranceNumber;
    @NotNull
    @JsonProperty("isOffLine")
    private Boolean isOffLine;
    @NotNull
    @JsonProperty("termsAndConditionAgreed")
    private Boolean termsAndConditionAgreed;
    @NotNull
    @JsonProperty("signatureCaptured")
    private Boolean signatureCaptured;
    @JsonProperty("datePrivacyAgreed")
    private Boolean datePrivacyAgreed;
    @NotNull
    @JsonProperty("createdFrom")
    private String createdFrom;
    @NotNull
    @JsonProperty("under18")
    private Boolean under18;
    @NotNull
    @JsonProperty("exemptedFromPayingCopayment")
    private Boolean exemptedFromPayingCopayment;
    @NotNull
    @JsonProperty("prepaymentChoice")
    private Integer prepaymentChoice;
    @JsonProperty("paymentMethod")
    private String paymentMethod;
    @NotNull
    @JsonProperty("measurement")
    private String measurement;
    @Size(max = 255)
    @JsonProperty("samplingProgramCode")
    private String samplingProgramCode;
    @JsonProperty("samplingOrderPlaced")
    private Boolean samplingOrderPlaced;
    @JsonProperty("isSubscribedForVatReduction")
    private Boolean isSubscribedForVatReduction;
    @JsonProperty("vatReductionStartDate")
    private String vatReductionStartDate;
    @JsonProperty("vatReductionEnddate")
    private String vatReductionEnddate;
    @JsonProperty("dniInf")
    private String dniInf;
    @JsonProperty("taxVatNumber")
    private String taxVatNumber;
    @Size(max = 255)
    @JsonProperty("nif")
    private String nif;
    @Size(max = 255)
    @JsonProperty("nip")
    private String nip;
    @JsonProperty("privacyConsent")
    private Boolean privacyConsent;
    @JsonProperty("privacySurvey")
    private Boolean privacySurvey;
    @JsonProperty("addresses")
    private List<Address> addresses = null;
    @JsonProperty("communication")
    private Communication communication;
}
