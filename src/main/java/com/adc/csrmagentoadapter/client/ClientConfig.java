package com.adc.csrmagentoadapter.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@RefreshScope
@Configuration
@EnableAutoConfiguration
@EnableFeignClients(basePackages = "com.adc.csrmagentoadapter.client")
public class ClientConfig {

    @Value("${url:default}")
    private String url;

    @Bean(name = "MAGENTO_URL")
    public String setUrl() {
        return this.url;
    }
}
