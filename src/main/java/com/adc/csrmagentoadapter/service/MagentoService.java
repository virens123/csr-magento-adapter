package com.adc.csrmagentoadapter.service;

import com.adc.csrmagentoadapter.domain.object.Address;
import com.adc.csrmagentoadapter.domain.object.Communication;
import com.adc.csrmagentoadapter.domain.object.Customer;

public interface MagentoService {

    public String insertCustomerDetails(Customer customer);
    public String upsertCustomerDetails(Customer customer);
    public String insertAddressDetails(Address address);
    public String upsertAddressDetails(Address address);
    public String upsertCommunicationDetails(Communication communication);
}
