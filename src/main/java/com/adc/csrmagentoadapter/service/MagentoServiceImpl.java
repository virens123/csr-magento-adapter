package com.adc.csrmagentoadapter.service;

import com.adc.csrmagentoadapter.client.MagentoClient;
import com.adc.csrmagentoadapter.domain.object.Address;
import com.adc.csrmagentoadapter.domain.object.Communication;
import com.adc.csrmagentoadapter.domain.object.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component(value = "magentoService")
public class MagentoServiceImpl implements MagentoService {
    @Autowired
    private MagentoClient magentoClient;

    public String insertCustomerDetails(Customer customer ) {
        //will be invoking the actual Magento service once ready
        return magentoClient.invokeMagento();
    }

    public String upsertCustomerDetails(Customer customer ) {
        //will be invoking the actual Magento service once ready
        return magentoClient.invokeMagento();
    }

    public String insertAddressDetails(Address address) {
        //will be invoking the actual Magento service once ready
        return magentoClient.invokeMagento();
    }

    public String upsertAddressDetails(Address address) {
        //will be invoking the actual Magento service once ready
        return magentoClient.invokeMagento();
    }

    public String upsertCommunicationDetails(Communication communication){
        return magentoClient.invokeMagento();
    }
}
