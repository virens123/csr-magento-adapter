package com.adc.csrmagentoadapter.controller;

import com.adc.csrmagentoadapter.domain.object.Address;
import com.adc.csrmagentoadapter.domain.object.Communication;
import com.adc.csrmagentoadapter.domain.object.Customer;
import com.adc.csrmagentoadapter.exception.CsrMagentoException;
import com.adc.csrmagentoadapter.service.MagentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RefreshScope
@RestController
@RequestMapping(value = "/{country}/{language}/v1.0/webshop")
public class CsrMagentoController {

    @Value("${message:Hello default}")
    private String message;

    @Value("${url: default}")
    private String url;

    @Autowired
    private MagentoService magentoService;

    @PostMapping(value = "/customers", produces = MediaType.TEXT_PLAIN_VALUE)
    public String insertCustomerDetails(@PathVariable(value= "country") String country ,@PathVariable(value= "language") String language,@RequestHeader(value = "consumerKey") String consumerKey, @RequestHeader(value = "accessToken") String accessToken, @Valid @RequestBody Customer customer) throws CsrMagentoException {
        try {
            System.out.println("message:" + message + "url :" + url);
            return magentoService.insertCustomerDetails(customer);
        } catch (Exception e) {
            throw new CsrMagentoException(e.getMessage());
        }
        //return this.message;
    }

    @PutMapping(value = "customers/{webshopCustomerId}", produces = MediaType.TEXT_PLAIN_VALUE)
    public String upsertCustomerDetails(@PathVariable(value= "country") String country ,@PathVariable(value= "language") String language,@PathVariable(value = "webshopCustomerId") String webshopCustomerId, @RequestHeader(value = "consumerKey") String consumerKey, @RequestHeader(value = "accessToken") String accessToken, @Valid @RequestBody Customer customer) throws CsrMagentoException {
        try {
            return magentoService.upsertCustomerDetails(customer);
        } catch (Exception e) {
            throw new CsrMagentoException(e.getMessage());
        }
    }

    @PutMapping(value = "customers/{webshopCustomerId}/communication", produces = MediaType.TEXT_PLAIN_VALUE)
    public String upsertCustomerDetails(@PathVariable(value= "country") String country ,@PathVariable(value= "language") String language,@PathVariable(value = "webshopCustomerId") String webshopCustomerId, @RequestHeader(value = "consumerKey") String consumerKey, @RequestHeader(value = "accessToken") String accessToken, @Valid @RequestBody Communication communication) throws CsrMagentoException {
        try {
            return magentoService.upsertCommunicationDetails(communication);
        } catch (Exception e) {
            throw new CsrMagentoException(e.getMessage());
        }
    }

    @PostMapping(value = "customers/{webshopCustomerId}/addresses", produces = MediaType.TEXT_PLAIN_VALUE)
    public String insertAddressDetails(@PathVariable(value= "country") String country ,@PathVariable(value= "language") String language,@PathVariable(value = "webshopCustomerId") String webshopCustomerId, @RequestHeader(value = "consumerKey") String consumerKey, @RequestHeader(value = "accessToken") String accessToken, @Valid @RequestBody Address address) throws CsrMagentoException {
        try {
            return magentoService.insertAddressDetails(address);
        } catch (Exception e) {
            throw new CsrMagentoException(e.getMessage());
        }
        //return client.helloWorld();
    }

    @PutMapping(value = "customers/{webshopCustomerId}/addresses/{webshopAddressId}", produces = MediaType.TEXT_PLAIN_VALUE)
    public String upsertAddressDetails(@PathVariable(value= "country") String country ,@PathVariable(value= "language") String language,@PathVariable(value = "webshopCustomerId") String webshopCustomerId, @PathVariable(value = "webshopAddressId") String webshopAddressId, @RequestHeader(value = "consumerKey") String consumerKey, @RequestHeader(value = "accessToken") String accessToken, @Valid @RequestBody Address address) throws CsrMagentoException {
        try {
            return magentoService.insertAddressDetails(address);
        } catch (Exception e) {
            throw new CsrMagentoException(e.getMessage());
        }
    }

    public String getUrl() {
        return url;
    }

}
