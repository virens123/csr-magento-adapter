package com.adc.csrmagentoadapter.exception;

public class CsrMagentoException extends Exception {

    public CsrMagentoException(String message){
    super(message);
    }

    public CsrMagentoException(String message, Throwable cause){
        super(message,cause);
    }

}
